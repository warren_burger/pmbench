﻿/*
   Copyright (c) 2016, University of Nevada, Las Vegas
   All rights reserved.
  
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
      1. Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.
   
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;

namespace PmGraphSpace
{
    public partial class ImportPmbenchException : Exception { }
    public partial class NullXmlNodeException : Exception { }

    public partial class BenchRound
    {
        public XmlNode roundNode; //XML node containing the trial in question, should be of type test_round
        public BenchSiblings seriesObject; //series (with identical params) this round belongs to
        public BenchPivot.PivotChart myPivotChart; //chart with histograms for transcription
        public int trialNum; //which trial number among its series
        public bool dirtyDelta; //some windows result files have impossibly large memory values from (fixed) output of negatives as unsigned
        public string customName;
        private bool readDeleteFlag { get; set; }
        private bool writeDeleteFlag { get;  set; }
        public bool killMeFlag { get; set; }
        public bool flaggedForAverage { get; set; }
        public bool hasPendingDeletions { get; set; }
        public bool hasReadSeries { get; set; }
        public bool hasWriteSeries { get; set; }
        private string readSeriesName, writeSeriesName;
        public bool wasDeletedDontBother;

        public bool setDeletionFlag(BenchPivot.AccessType type)
        {
            switch (type)
            {
                case (BenchPivot.AccessType.read):
                    if (hasReadSeries) return hasPendingDeletions = readDeleteFlag = true;
                    break;
                case (BenchPivot.AccessType.write):
                    if (hasWriteSeries) return hasPendingDeletions = writeDeleteFlag = true;
                    break;
                default:
                    MessageBox.Show("BenchRound.setDeletionFlag error");
                    break;
            }
            return false;
        }

        public bool unsetDeletionFlag(BenchPivot.AccessType type)
        {
            if (hasPendingDeletions)
            {
                switch (type)
                {
                    case (BenchPivot.AccessType.read):
                        if (hasReadSeries) readDeleteFlag = false;
                        break;
                    case (BenchPivot.AccessType.write):
                        if (hasReadSeries) writeDeleteFlag = false;
                        break;
                    default:
                        MessageBox.Show("BenchRound.unsetDeletionFlag access type error");
                        return true;
                }
                return (hasPendingDeletions = (readDeleteFlag | writeDeleteFlag));
            }
            MessageBox.Show("BenchRound.unsetDeletionFlag pending deletion flag error");
            return false;
        }

        public BenchRound()
        {
            roundNode = null;
            seriesObject = null;
            myPivotChart = null;
            readDeleteFlag = false;
            writeDeleteFlag = false;
            killMeFlag = false;
            readSeriesName = null;
            writeSeriesName = null;
            flaggedForAverage = false;
            customName = null;
            hasPendingDeletions = false;
            hasReadSeries = false;
            hasWriteSeries = false;
        }

        public BenchRound(BenchSiblings bs, XmlNode node, int i)
        {
            roundNode = node;
            seriesObject = bs;
            trialNum = i;
        }

        private static XmlNode safeSelectSingleNode(XmlNode n, string s) { return PmGraph.safeSelectSingleNode(n, s); }
        private static int safeParseSingleNodeInt(XmlNode n, string s) { return PmGraph.safeParseSingleNodeInt(n, s); }
        private static long safeParseSingleNodeLong(XmlNode n, string s) { return PmGraph.safeParseSingleNodeLong(n, s); }
        private static double safeParseSingleNodeDouble(XmlNode n, string s) { return PmGraph.safeParseSingleNodeDouble(n, s); }
        public int jobs()
        {
            return seriesObject.benchParams.valueJobs;
        }
        public int ratio()
        {
            return seriesObject.benchParams.valueRatio;
        }
        public string operatingSystem() { return seriesObject.benchParams.operatingSystem; }
        public string swapDevice() { return seriesObject.benchParams.swapDevice; }
        public int cold() { return seriesObject.benchParams.cold; }
        private string[] memstrings_w = { "AvailPhys", "dwMemoryLoad", "TotalPageFile", "AvailPageFile", "AvailVirtual" };
        public bool windowsbench() { return seriesObject.windowsbench; }

        public Chart getRoundChart(BenchPivot.DetailLevel detail)
        {
            if (myPivotChart == null) myPivotChart = new BenchPivot.PivotChart(roundNode);               
            return (myPivotChart.getPivotChart(detail));
        }
        
        public void registerSeriesName(string s, BenchPivot.AccessType t)
        {
            if (s == null) return;
            switch (t)
            {
                case (BenchPivot.AccessType.read):
                    if (!hasReadSeries)
                    {
                        readSeriesName = s;
                        hasReadSeries = true;
                    }
                    break;
                case (BenchPivot.AccessType.write):
                    if (!hasWriteSeries)
                    {
                        writeSeriesName = s;
                        hasWriteSeries = true;
                    }
                    break;
                default:
                    MessageBox.Show("BenchRound.registerSeriesName error");
                    return;
            }
        }

        private double totalSamples = -1;
        public double getTotalSamples()
        {
            if (totalSamples < 0)
            {
                totalSamples = 0;
                XmlNodeList samplecounts = this.roundNode.SelectNodes("pmbenchmark/report/result/result_thread/result_details/details_samples");
                for (int i = 0; i < samplecounts.Count; i++)
                {
                    totalSamples += double.Parse(samplecounts[i].InnerText);
                }
            }
            return totalSamples;
        }

        public int deleteSelectedSeries()
        {
            int deleted = 0;
            if (!hasPendingDeletions) return 0;
            Chart shortChart = myPivotChart.getPivotChart(BenchPivot.DetailLevel.shorttype);
            Chart fullChart = myPivotChart.getPivotChart(BenchPivot.DetailLevel.fulltype);
            if (readDeleteFlag)
            {
                if (hasReadSeries)
                {
                    shortChart.Series.Remove(shortChart.Series.FindByName("read"));
                    fullChart.Series.Remove(fullChart.Series.FindByName("read"));
                    hasReadSeries = false;
                    deleted += 1;
                }
            }
            if (writeDeleteFlag)
            {
                if (hasWriteSeries)
                {
                    shortChart.Series.Remove(shortChart.Series.FindByName("write"));
                    fullChart.Series.Remove(fullChart.Series.FindByName("write"));
                    hasWriteSeries = false;
                    deleted += 1;
                }
            }
            if (deleted > 0)
            {
                hasPendingDeletions = false;
                wasDeletedDontBother = true;
                return deleted;
            }
            else
            {
                MessageBox.Show("BenchRound.deleteSelected error");
                return 0;
            }
        }
    }

    public partial class BenchSiblings //a set of benchmarks with common parameters
    {
        public XmlNode seriesNode, averageNode; //XML nodes for the series root, and the node containing series averages
        public List<BenchRound> Trials; //rounds from this series
        public XmlDocument theDoc; //hosting XML document
        public ParamSet benchParams; //this series' params
        public BenchRound averageRound; //the BenchRound object, not to be confused with its XML node
        private delegate void addMemItemsDelegate(XmlNode item_avg, XmlNode item_, int i, int trials); //I just wanted to try these out
        public bool windowsbench = false; //temporary fix for some windows benchmarks
        public int trialsPerSeries = -1;
        public double samplesPerThread, netAverageLatency, readSpike, writeSpike, readSpikeHexVal, writeSpikeHexVal, readSamplesTotal, writeSamplesTotal; //average samples per thread, net average acces latency, highest latency count in read and write histograms, and total samples
        public int readSpikeIntervalHi = -1, writeSpikeIntervalHi = -1, readSpikeHexBinNum = -1, writeSpikeHexBinNum = -1; //for locating the read and write spikes
        public bool spikesCalculated = false;
        public bool initialized;

        public BenchRound getAverageRound()
        {
            if (averageRound == null)
            {
                if (getAverageNode() == null)
                {
                    MessageBox.Show("(BenchRound.getAverageRound) Error: still unable to create average node");
                    return null;
                }
                averageRound = new BenchRound(this, getAverageNode(), trialsPerSeries + 1);
                calculateSpikes();
                this.Trials.Add(averageRound);
            }
            return (averageRound);
        }

        private void calculateSpikes()
        {
            if (this.spikesCalculated)
            {
                MessageBox.Show("calculateSpikes error: Spikes already calculated!");
            }
            XmlNodeList readbuckets = averageNode.SelectNodes("pmbenchmark/report/statistics/histogram[@type='read']/histo_bucket");
            XmlNodeList writebuckets = averageNode.SelectNodes("pmbenchmark/report/statistics/histogram[@type='write']/histo_bucket");
            if (readbuckets.Count == 0 && writebuckets.Count == 0)
            {
                //MessageBox.Show("calculateSpikes error: both histograms are null");
                throw new ImportPmbenchException();
            }
            readSpike = 0;
            writeSpike = 0;
            readSpikeHexVal = 0;
            writeSpikeHexVal = 0;
            for (int i = 0; i < readbuckets.Count; i++)
            {
                double tempSpike = safeParseSingleNodeDouble(readbuckets[i], "sum_count");
                if (tempSpike > readSpike)
                {
                    readSpike = tempSpike;
                    readSpikeIntervalHi = safeParseSingleNodeInt(readbuckets[i], "bucket_interval/interval_hi");
                }
                if (readbuckets[i].Attributes.Item(0).Name.Equals("index") && !readbuckets[i].Attributes.Item(0).Value.Equals("0"))
                {
                    for (int j = 0; j < 16; j++)
                    {
                        double hexbin = safeParseSingleNodeDouble(readbuckets[i], "bucket_hexes/hex[@index='" + j + "']");
                        if (hexbin > readSpikeHexVal)
                        {
                            readSpikeHexVal = hexbin;
                            readSpikeHexBinNum = j;
                        }
                    }
                }
                tempSpike = safeParseSingleNodeDouble(writebuckets[i], "sum_count");
                if (tempSpike > writeSpike)
                {
                    writeSpike = tempSpike;
                    writeSpikeIntervalHi = safeParseSingleNodeInt(writebuckets[i], "bucket_interval/interval_hi");
                }
                if (writebuckets[i].Attributes.Item(0).Name.Equals("index") && !writebuckets[i].Attributes.Item(0).Value.Equals("0"))
                {
                    for (int j = 0; j < 16; j++)
                    {
                        double hexbin = safeParseSingleNodeDouble(writebuckets[i], "bucket_hexes/hex[@index='" + j + "']");
                        if (hexbin > writeSpikeHexVal)
                        {
                            writeSpikeHexVal = hexbin;
                            writeSpikeHexBinNum = j;
                        }
                    }
                }
            }

            XmlNodeList netavgs = averageNode.SelectNodes("pmbenchmark/report/result/result_thread/result_netavg/netavg_us");
            XmlNodeList samples = averageNode.SelectNodes("pmbenchmark/report/result/result_thread/result_details/details_samples");
            samplesPerThread = 0;
            netAverageLatency = 0;
            for (int i = 0; i < netavgs.Count; i++)
            {
                netAverageLatency += double.Parse(netavgs[i].InnerText.ToString());
                samplesPerThread += double.Parse(samples[i].InnerText.ToString());
            }
            netAverageLatency /= netavgs.Count;
            samplesPerThread /= netavgs.Count;
            this.spikesCalculated = true;
        }

        public void displaySpikes()
        {
            MessageBox.Show("Net average latency:\t" + netAverageLatency + "\nAverage samples per thread:\t" + samplesPerThread + "\nRead spike:\t" + readSpike + " at bucket 2^" + readSpikeIntervalHi + "; " + readSpikeHexVal + " at bin " + readSpikeHexBinNum + "\nWrite spike:\t" + writeSpike + " at bucket 2^" + writeSpikeIntervalHi + "; " + writeSpikeHexVal + " at bin " + writeSpikeHexBinNum);
        }

        public BenchSiblings()
        {
            this.seriesNode = null;
            this.Trials = null;
            this.theDoc = null;
            this.benchParams = null;
            this.averageNode = null;
            this.averageRound = null;
            this.initialized = false;
        }

        public BenchSiblings(XmlNode node, XmlDocument doc, ParamSet bp)
        {
            this.initialized = false;
            seriesNode = node;
            theDoc = doc;
            benchParams = bp;
            try
            {
                if (benchParams == null) { throw new NullReferenceException("Null benchparams, assuming this is a windows benchmark you're importing"); }
                else windowsbench = benchParams.operatingSystem.Contains("Windows");
            }
            catch (NullReferenceException x)
            {
                MessageBox.Show(x.ToString());
                return;
            }
            Trials = new List<BenchRound>();
            XmlNodeList test_rounds = seriesNode.SelectNodes("test_round");
            this.trialsPerSeries = test_rounds.Count;
            for (int i = 0; i < test_rounds.Count; i++)
            {
                BenchRound br = new BenchRound(this, test_rounds.Item(i), i + 1);
                Trials.Add(br);
            }
            test_rounds = null;
            try
            {
                if (getAverageRound() == null)
                {
                    MessageBox.Show("(BenchSiblings(XmlNode, XmlDocument, ParamSet) Error: Unable to generate average round");
                    return;
                }
                this.initialized = true;
            }
            catch (ImportPmbenchException) // x)
            {
                return;
            }
        }

        private static XmlNode safeSelectSingleNode(XmlNode n, string s) { return PmGraph.safeSelectSingleNode(n, s); }
        private static int safeParseSingleNodeInt(XmlNode n, string s) { return PmGraph.safeParseSingleNodeInt(n, s); }
        private static double safeParseSingleNodeDouble(XmlNode n, string s) { return PmGraph.safeParseSingleNodeDouble(n, s); }

        public XmlNode getAverageNode() //get the node of the trial containing averages
        {
            if (averageNode == null)
            {
                averageNode = makeAverageNode();
            }
            return averageNode;
        }

        private XmlNode partialCloneBenchmark(int trials) //clone the first benchmark's result, statistics and sys_mem_info and insert the partial clone as last child of parent
        {
            try
            {
                XmlNode avg = null, parent = this.seriesNode;
                XmlDocument doc = this.theDoc;
                if (doc == null) doc = parent.OwnerDocument;
                XmlNode result1, statistics, sys_mem_info, report_temp, pmb_temp;
                string report_s = "test_round[@iter='1']/pmbenchmark/report/";
                result1 = safeSelectSingleNode(parent, report_s + "result").Clone();
                statistics = safeSelectSingleNode(parent, report_s + "statistics").Clone();
                sys_mem_info = safeSelectSingleNode(parent, report_s + "sys_mem_info").Clone();
                report_temp = doc.CreateNode(XmlNodeType.Element, "report", doc.NamespaceURI); //IMPORTANT: using the wrong namespace causes impossibilities like node.SelectSingleNode(node.FirstChild.Name) returning null
                report_temp.AppendChild(result1);
                report_temp.AppendChild(statistics);
                report_temp.AppendChild(sys_mem_info);
                pmb_temp = doc.CreateNode(XmlNodeType.Element, "pmbenchmark", doc.NamespaceURI);
                pmb_temp.AppendChild(report_temp);
                avg = doc.CreateNode(XmlNodeType.Element, "test_round", doc.NamespaceURI);
                XmlAttribute iter = doc.CreateAttribute("iter");
                iter.Value = (trials + 1).ToString();
                avg.Attributes.Append(iter);
                avg.AppendChild(pmb_temp);
                return avg;
            }
            catch (NullReferenceException x)
            {
                MessageBox.Show("partialCloneBenchmark null reference exception: " + x.ToString());
                return null;
            }
        }

        private static void addMemItemField(XmlNode item_avg, XmlNode item_, int i, string s, int trials) //works for some elements in result as well
        {
            try
            {
                double t = safeParseSingleNodeDouble(item_avg, s);
                t += safeParseSingleNodeDouble(item_, s);
                if (i == trials) { t /= (float)trials; }
                safeSelectSingleNode(item_avg, s).InnerText = t.ToString();
            }
            catch (NullReferenceException x) { MessageBox.Show("(addMemItemField) Adding field " + s + " for round " + i + ":\n" + x.ToString()); }
        }

        private static void addThreadResults(XmlNode thread_avg, XmlNode thread_, int i, int trials) //add thread 2's result data to thread 1's, then divide by 5 if i == 5
        {
            try
            {
                addMemItemField(thread_avg, thread_, i, "result_netavg/netavg_us", trials);
                addMemItemField(thread_avg, thread_, i, "result_netavg/netavg_clk", trials);
                addMemItemField(thread_avg, thread_, i, "result_details/details_latency/latency_us", trials);
                addMemItemField(thread_avg, thread_, i, "result_details/details_latency/latency_clk", trials);
                addMemItemField(thread_avg, thread_, i, "result_details/details_samples", trials);
                addMemItemField(thread_avg, thread_, i, "result_details/details_overhead/overhead_us", trials);
                addMemItemField(thread_avg, thread_, i, "result_details/details_overhead/overhead_clk", trials);
                addMemItemField(thread_avg, thread_, i, "result_details/details_total", trials);
            }
            catch (NullReferenceException) // x)
            {
                MessageBox.Show("addThreadResults error: node " + i + " of " + trials + " has malformed or missing per-thread results. Averages will not be correct.");
                return;
            }
        }

        public static bool addHistograms(XmlNode histo_avg, XmlNode histo, int round, int trials) //add the second histogram to the first, then divide if it's round 5
        {
            XmlNode bucket, bucket_avg;
            double sum_temp, term_temp;
            for (int j = 1; j <= 15; j++)
            {
                //get node 6's bucket
                try { bucket_avg = safeSelectSingleNode(histo_avg, "histo_bucket[@index='" + j + "']"); }
                catch (Exception x)
                {
                    MessageBox.Show("Error: selecting bucket " + j + " in histo_avg:\n" + x.ToString());
                    return false;
                }
                //get test node's bucket
                    try { bucket = safeSelectSingleNode(histo, "histo_bucket[@index='" + j + "']"); }
                    catch (Exception x) { MessageBox.Show("Error: selecting bucket " + j + " in histo:\n" + x.ToString()); return false; }
                //get sum_count from node 6's bucket
                    try { sum_temp = safeParseSingleNodeDouble(bucket_avg, "sum_count"); }
                    catch (Exception x) { MessageBox.Show("Error: retrieving/parsing sum_temp of bucket_avg " + j + ":\n" + x.ToString()); return false; }
                //add sum_count from test node's bucket
                    try { term_temp = safeParseSingleNodeDouble(bucket, "sum_count"); }
                    catch (Exception x) { MessageBox.Show("Error: parsing/adding sum_temp of bucket " + j + ":\n" + x.ToString()); return false; }
                sum_temp += term_temp;
                if (round == trials)
                {
                    sum_temp /= trials;
                }
                //update node 6's bucket
                try { safeSelectSingleNode(bucket_avg, "sum_count").InnerText = sum_temp.ToString(); }
                catch (Exception x) { MessageBox.Show("Error updating sum for bucket " + j + " in histo_avg:\n" + x.ToString()); return false; }

                for (int k = 0; k < 16; k++)
                {
                    try
                    {
                        addMemItemField(bucket_avg, bucket, round, "bucket_hexes/hex[@index='" + k + "']", trials);
                    }
                    catch (Exception x) { MessageBox.Show("Error adding hex " + k + " in bucket " + j + ":\n" + x.ToString()); return false; }
                }
            }
            //fix for a poor choice I made in writing the XML
            XmlNodeList bucket0, bucket0_avg;
            try
            {
                bucket0_avg = histo_avg.SelectNodes("bucket_hexes/hex[@index='0']");
                bucket0 = histo.SelectNodes("bucket_hexes/hex[@index='0']");
            }
            catch (System.Xml.XPath.XPathException x) { MessageBox.Show("Error getting nodes for bucket0:\n" + x.ToString()); return false; }
            if (bucket0.Count != bucket0_avg.Count)
            {
                MessageBox.Show("Error: somehow, current node and average have a different number of bucket0's"); return false;
            }
            for (int j = 0; j < bucket0.Count; j++)
            {
                try
                {
                    bucket_avg = bucket0_avg.Item(j); //why can't you access these like an array?
                    bucket = bucket0.Item(j);
                    addMemItemField(bucket_avg, bucket, round, "sum_count", trials);
                }
                catch (Exception x) { MessageBox.Show("Error updating sum count for " + j + "th bucket 0:\n" + x.ToString()); return false; }
            }
            return true;
        }

        private static void addMemItemsLinux(XmlNode item_avg, XmlNode item_, int i, int trials)
        {
            try
            {
                addMemItemField(item_avg, item_, i, "free_kib", trials);
                addMemItemField(item_avg, item_, i, "buffer_kib", trials);
                addMemItemField(item_avg, item_, i, "cache_kib", trials);
                addMemItemField(item_avg, item_, i, "active_kib", trials);
                addMemItemField(item_avg, item_, i, "inactive_kib", trials);
                addMemItemField(item_avg, item_, i, "pgpgin", trials);
                addMemItemField(item_avg, item_, i, "pgpgout", trials);
                addMemItemField(item_avg, item_, i, "pswpin", trials);
                addMemItemField(item_avg, item_, i, "pswpout", trials);
                addMemItemField(item_avg, item_, i, "pgmajfault", trials);
            }
            catch (NullReferenceException) // x)
            {
                MessageBox.Show("(addMemItemsLinux) Null reference ");
            }
        }
        private static void addMemItemsWindows(XmlNode item_avg, XmlNode item_, int i, int trials)
        {
            addMemItemField(item_avg, item_, i, "AvailPhys", trials);
            addMemItemField(item_avg, item_, i, "dwMemoryLoad", trials);
            addMemItemField(item_avg, item_, i, "TotalPageFile", trials);
            addMemItemField(item_avg, item_, i, "AvailPageFile", trials);
            addMemItemField(item_avg, item_, i, "AvailVirtual", trials);
        }

        private XmlNode makeAverageNode() //do something with the average of some benchmarks
        {
            if (this.trialsPerSeries == 1) { return this.Trials[0].roundNode; }
            if (averageNode != null) { MessageBox.Show("(BenchSiblings.makeAverage) Warning: Attempted to insert average node for a series that already has one"); return averageNode; }
            int ratio, jobs, cold;
            XmlNode histo;
            XmlNode histo_avg;
            XmlNode result_;
            XmlNode result_avg;
            XmlNode thread_avg;
            XmlNode thread_;
            XmlNode item_;
            XmlNode item_avg;
            XmlNodeList sys_mem_items_, sys_mem_items_avg;
            addMemItemsDelegate addMemItems;
            if (windowsbench) { addMemItems = addMemItemsWindows; } else { addMemItems = addMemItemsLinux; }
            string report_s = "pmbenchmark/report";
            try
            {
                string params_s = "test_round/" + report_s + "/signature/params/";
                ratio = safeParseSingleNodeInt(seriesNode, params_s + "ratio");
                jobs = safeParseSingleNodeInt(seriesNode, params_s + "jobs");
                cold = safeParseSingleNodeInt(seriesNode, params_s + "cold");
            }
            catch (Exception x) { MessageBox.Show("(BenchSiblings.makeAverageNode) Error: unable to retrieve ratio parameter:\n" + x.ToString()); return null; }
            XmlNode avg = partialCloneBenchmark(trialsPerSeries);
            if (avg == null)
            {
                MessageBox.Show("makeAverageNode error: cloned average returned as null.");
                return null;
            }
            seriesNode.AppendChild(avg);

            string result_s = report_s + "/result";
            string meminfo_s = report_s + "/sys_mem_info/sys_mem_item";
            result_avg = safeSelectSingleNode(avg, result_s);
            sys_mem_items_avg = avg.SelectNodes(meminfo_s);
            string histo_s = report_s + "/statistics/histogram";
            string round_s;
            for (int i = 2; i < this.trialsPerSeries + 1; i++)
            {
                round_s = "test_round[@iter = '" + i + "']";
                //average the individual thread results
                result_ = safeSelectSingleNode(seriesNode, round_s + "/" + result_s);
                try
                {
                    for (int j = 1; j <= jobs; j++)
                    {
                        thread_avg = safeSelectSingleNode(result_avg, "result_thread[@thread_num='" + j + "']");
                        thread_ = safeSelectSingleNode(result_, "result_thread[@thread_num='" + j + "']");
                        addThreadResults(thread_avg, thread_, i, trialsPerSeries);
                    }
                }
                catch (NullReferenceException) // x)
                {
                    MessageBox.Show("addThreadResults error: node " + i + " of " + trialsPerSeries + " has malformed or missing per-thread results. Averages will not be correct.");
                    continue;
                }
                //average the histograms
                if (ratio > 0) //deal with read histogram here
                {
                    histo_avg = safeSelectSingleNode(avg, histo_s + "[@type='read']");
                    histo = safeSelectSingleNode(seriesNode, round_s + "/" + histo_s + "[@type='read']");
                    if (!addHistograms(histo_avg, histo, i, trialsPerSeries)) { MessageBox.Show("(BenchSiblings.makeAverageNode) Error adding read histograms"); return null; }
                }
                if (ratio < 100)
                {
                    histo_avg = safeSelectSingleNode(avg, histo_s + "[@type='write']");
                    if (histo_avg == null)
                    {
                        MessageBox.Show("(BenchSiblings.makeAverageNode) This series of benches (at " + avg.Name + ") have no write histograms at " + histo_s + "[@type='write']" + ", apparently ");
                        return null;
                    }
                    histo = safeSelectSingleNode(seriesNode, round_s + "/" + histo_s + "[@type='write']");
                    if (!addHistograms(histo_avg, histo, i, trialsPerSeries)) { MessageBox.Show("(BenchSiblings.makeAverageNode) Error adding write histograms"); return null; }
                }
                //sys_mem_items
                sys_mem_items_ = seriesNode.SelectNodes(round_s + "/" + meminfo_s);
                for (int j = 0; j < sys_mem_items_avg.Count; j++)
                {
                    item_avg = safeSelectSingleNode(sys_mem_items_avg.Item(j), "mem_item_info");
                    item_ = safeSelectSingleNode(sys_mem_items_.Item(j), "mem_item_info");
                    addMemItems(item_avg, item_, i, trialsPerSeries);
                    if (j != sys_mem_items_avg.Count - 1)
                    {
                        item_avg = safeSelectSingleNode(sys_mem_items_avg.Item(j), "mem_item_delta");
                        item_ = safeSelectSingleNode(sys_mem_items_.Item(j), "mem_item_delta");
                        addMemItems(item_avg, item_, i, trialsPerSeries);
                    }
                }
            }
            return avg;
        }

    }

    public partial class ParamSet
    {
        public int valueJobs, valueRatio;
        public string operatingSystem, swapDevice;
        public int duration, setsize, quiet, cold, offset;
        public string shape, pattern, access, tsops;

        public ParamSet()
        {
            this.operatingSystem = null;
            this.swapDevice = null;
            this.shape = null;
            this.pattern = null;
            this.access = null;
            this.tsops = null;
        }

        private static XmlNode safeSelectSingleNode(XmlNode n, string s) { return PmGraph.safeSelectSingleNode(n, s); }
        private static int safeParseSingleNodeInt(XmlNode n, string s) { return PmGraph.safeParseSingleNodeInt(n, s); }
        private static double safeParseSingleNodeDouble(XmlNode n, string s) { return PmGraph.safeParseSingleNodeDouble(n, s); }

        public void setParamsFromNode(XmlNode p) //This is ALSO not a constructor
        {
            if (p == null) { MessageBox.Show("ParamSet.setParamsFromNode: Error: received null input node"); return; }
            this.duration = safeParseSingleNodeInt(p, "duration");
            this.setsize = safeParseSingleNodeInt(p, "setsize");
            this.valueJobs = safeParseSingleNodeInt(p, "jobs");
            this.valueRatio = safeParseSingleNodeInt(p, "ratio");
            this.shape = safeSelectSingleNode(p, "shape").InnerText;
            this.quiet = safeParseSingleNodeInt(p, "quiet");
            this.cold = safeParseSingleNodeInt(p, "cold");
            this.offset = safeParseSingleNodeInt(p, "offset");
            this.pattern = safeSelectSingleNode(p, "pattern").InnerText;
            this.access = safeSelectSingleNode(p, "access").InnerText;
            this.tsops = safeSelectSingleNode(p, "tsops").InnerText;
        }
    }

    public partial class BenchPivot //a benchmark with comparisons to some other stuff
    {
        public partial class PivotChart
        {
            public partial class BetterSeries
            {
                protected Series shortSeries, fullSeries, currentSeries;
                protected Point chartPoint;
                protected bool showFull { set; get; }
                private string seriesName;
                private bool selected, grayed;
                protected Color backupColor;
                static Color unselectedColor = Color.FromArgb(32, 32, 32, 32);
                public BenchRound theBenchRound { set; get; }
                private AccessType myAccessType;

                public bool getSelected() { return selected; }

                public AccessType getMyAccessType() { return myAccessType; }

                public void updateSelectionColor(int sel)
                {
                    if (!selected && sel > 0)
                    {
                        if (!grayed)
                        {
                            saveBackupColor(shortSeries.Color);
                            setColor(unselectedColor);
                            grayed = true;
                        }
                    }
                    else
                    {
                        setColor(backupColor);
                        saveBackupColor(setColor(backupColor));
                        grayed = false;
                    }
                }

                public string deleteFlagYourselfIfSelected()
                {
                    if (!selected) return null;
                    selected = false;
                    theBenchRound.setDeletionFlag(this.myAccessType);
                    setSeriesEnabled(false);
                    return seriesName;
                }

                public string undeleteYourself()
                {
                    theBenchRound.unsetDeletionFlag(this.myAccessType);
                    setSeriesEnabled(true);
                    return seriesName;
                }

                public string finalizeDeletion(Chart sc, Chart fc)
                {
                    if (!selected) return null;
                    sc.Series.Remove(shortSeries);
                    shortSeries.Dispose();
                    shortSeries = null;
                    fc.Series.Remove(fullSeries);
                    fullSeries.Dispose();
                    fullSeries = null;
                    currentSeries = null;
                    selected = false;
                    grayed = false;
                    string s = seriesName;
                    theBenchRound.setDeletionFlag(this.myAccessType);
                    theBenchRound = null;
                    return s;
                }

                public int toggleSelected(int currently)
                {
                    selected = !selected;
                    if (selected) theBenchRound.flaggedForAverage = true;
                    else if (currently > 1) updateSelectionColor(currently - 1);
                    return (selected ? 1 : -1);
                }

                public bool setFull(bool b)
                {
                    showFull = b;
                    currentSeries = (showFull ? fullSeries : shortSeries);
                    return showFull;
                }

                private Series refreshCurrentSeries()
                {
                    setFull(showFull);
                    return currentSeries;
                }

                public void setSeriesEnabled(bool set)
                {
                    shortSeries.Enabled = set;
                    fullSeries.Enabled = set;
                }

                public string getSeriesName()
                {
                    if (currentSeries == null) return seriesName;
                    return currentSeries.Name;
                }

                public Color saveBackupColor(Color c)
                {
                    backupColor = c;
                    return backupColor;
                }

                public Color setColor(Color c)
                {
                    shortSeries.Color = c;
                    fullSeries.Color = c;
                    return c;
                }

                public BetterSeries()
                {
                    shortSeries = null;
                    fullSeries = null;
                    currentSeries = null;
                    seriesName = null;
                    selected = false;
                    backupColor = unselectedColor;
                    grayed = false;
                    myAccessType = AccessType.uninitialized;
                }

                public Series setContainedSeries(Series series, AccessType type)
                {
                    switch (series.Points.Count)
                    {
                        case (250):
                            if (this.fullSeries == null) fullSeries = series;
                            else MessageBox.Show("BenchPivot.PivotChart.BetterSeries.setContainedSeries error: full series " + series.Name + " is already set");
                            break;
                        case (25):
                            if (this.shortSeries == null) shortSeries = series;
                            else MessageBox.Show("BenchPivot.PivotChart.BetterSeries.setContainedSeries error: short series is already set");
                            break;
                        default:
                            MessageBox.Show("BenchPivot.PivotChart.BetterSeries.setContainedSeries error: Found a series with " + series.Points.Count + " points, something went wrong");
                            return series;
                    }
                    if (seriesName == null)
                    {
                        seriesName = series.Name;
                        saveBackupColor(series.Color);
                        myAccessType = type;
                    }
                    return series;
                }
            }

            private partial class HoverSeries : BetterSeries
            {
                private Series initializeHoverSeries(Series s)
                {
                    s.ChartType = SeriesChartType.SplineArea;
                    s.Enabled = false;
                    s.IsVisibleInLegend = false;
                    return s;
                }

                public HoverSeries(PivotChart pivotchart)
                {
                    string hts = "hover_short", htf = "hover_full";
                    pivotchart.shortChart.Series.Add(hts);
                    shortSeries = initializeHoverSeries(pivotchart.shortChart.Series.FindByName(hts));
                    pivotchart.fullChart.Series.Add(htf);
                    fullSeries = initializeHoverSeries(pivotchart.fullChart.Series.FindByName(htf));
                    chartPoint = pivotchart.theBenchPivot.chartPoint;
                }
            }

            private Chart shortChart, fullChart;
            public Chart currentChart;
            private Point chartPoint;
            private HoverSeries hoverSeries;
            private BenchPivot theBenchPivot;
            private bool showFull { set; get; }
            private Random randomColorState;
            private Dictionary<string, BetterSeries> allSeries;
            private int selectionCount;
            public List<string> flaggedForDeletion;
            private Dictionary<string, BetterSeries> partnerSeries;

            public string getBetterSeriesName(BenchRound round, AccessType type)
            {
                string append = " (" + (type == AccessType.read ? "read" : "write") + ")";
                try
                {
                    return allSeries[round.customName + append].getSeriesName();
                }
                catch (ArgumentException)
                {
                    return null;
                }
            }

            public int getSelectionCount() { return selectionCount; }

            public int deleteFlagSelected(bool nag) //Intended to add an undelete option
            {
                Dictionary<string, BetterSeries>.ValueCollection.Enumerator checkus = allSeries.Values.GetEnumerator();
                string s = null;
                List<string> deleteus = new List<string>();
                while (checkus.MoveNext())
                {
                    s = checkus.Current.deleteFlagYourselfIfSelected();
                    if (s != null) flaggedForDeletion.Add(s);
                }
                selectionCount -= flaggedForDeletion.Count;
                return flaggedForDeletion.Count;
            }

            public int finalizeDeletions(int howmany)
            {
                int ret = flaggedForDeletion.Count;
                for (int i = 0; i < ret; i++)
                {
                    BetterSeries bs = allSeries[flaggedForDeletion[i]];
                    allSeries.Remove(flaggedForDeletion[i]);
                    bs.finalizeDeletion(shortChart, fullChart);
                }
                flaggedForDeletion.Clear();
                return ret;
            }

            private static void writeHexBinsToChart(XmlNode bucket, double interval_lo, double interval_hi, Chart c, AccessType type)
            {
                //get the midpoint between interval values
                string sname = (type == AccessType.read ? "read" : "write");
                double interval_ = (interval_hi - interval_lo) / 16;
                for (int j = 0; j < 16; j++) //graph it (involves retrieving bucket sub hex index), skipping nodes with no samples
                {
                    double hex = safeParseSingleNodeDouble(bucket, "bucket_hexes/hex[@index='" + j + "']");
                    //if (hex == 0) { continue; }
                    double xval = interval_lo + (0.5 + j) * interval_;
                    c.Series[sname].Points.AddXY(xval, hex);
                }
            }

            private static void writeSumCountOnlyToChart(XmlNode bucket, Chart c, AccessType type)
            {
                string sname = (type == AccessType.read ? "read" : "write");
                double sum_count = safeParseSingleNodeDouble(bucket, "sum_count");
                //if (sum_count == 0) { return; }
                double interval_lo = (double)safeParseSingleNodeInt(bucket, "bucket_interval/interval_lo");
                double interval_hi = (double)safeParseSingleNodeInt(bucket, "bucket_interval/interval_hi");
                double interval_ = (interval_hi - interval_lo);
                double xval = interval_lo + (interval_ / 2);
                c.Series[sname].Points.AddXY(xval, sum_count);
            }

            private static void writeHistogramToChart(Chart chart, XmlNode stats, AccessType type, Color color, bool full) //get the chart ready. Important for the Series that is produced.
            {
                string sname = (type == AccessType.read ? "read" : "write");
                XmlNode histogram = safeSelectSingleNode(stats, "histogram[@type='" + sname + "']");
                if (histogram != null)
                {
                    XmlNode bucket;
                    double interval_lo, interval_hi, sum_count;
                    XmlNodeList bucket0;

                    Series series = new Series();
                    series.ChartArea = (full ? "hex_bins" : "sum_count");
                    series.Legend = "Legend1";
                    series.Name = sname;
                    chart.Series.Add(series);

                    //get all histo_buckets with index 0 (for very large and very small latencies) and deal with the first one (< 2^8 ns)
                    bucket0 = histogram.SelectNodes("histo_bucket[@index='0']");
                    bucket = bucket0.Item(0);
                    sum_count = safeParseSingleNodeDouble(bucket, "sum_count");
                    chart.Series[sname].Points.AddXY(8, sum_count);
                    chart.Series[sname].Points.AddXY(8, sum_count);

                    for (int i = 1; i < 16; i++) //intentionally miscalculates x coordinate because (2^lo+(j+0.5)*((2^hi-2^lo)/16)) stretches the x axis
                    {
                        bucket = safeSelectSingleNode(histogram, "histo_bucket[@index='" + i + "']");
                        if (bucket == null)
                        {
                            MessageBox.Show("writeHistogramToChart error: could not select histo_bucket[@index='" + i + "']");
                            return;
                        }
                        if (full)
                        {
                            interval_lo = (double)safeParseSingleNodeInt(bucket, "bucket_interval/interval_lo");
                            interval_hi = (double)safeParseSingleNodeInt(bucket, "bucket_interval/interval_hi");
                            writeHexBinsToChart(bucket, interval_lo, interval_hi, chart, type);
                        }
                        else { writeSumCountOnlyToChart(bucket, chart, type); }
                    }

                    for (int j = 1; j < bucket0.Count; j++) //deal with the rest of the index 0 histo buckets
                    {
                        bucket = bucket0.Item(j);
                        writeSumCountOnlyToChart(bucket, chart, type);
                    }
                    chart.Series[sname].ChartType = SeriesChartType.FastLine;
                    chart.Series[sname].Color = color;
                    bucket = null;
                    bucket0 = null;
                }
                else
                {
                    MessageBox.Show("writeHistogramToChart: unable to write histogram");
                }
                histogram = null;
            }

            public PivotChart()
            {
                partnerSeries = new Dictionary<string, BetterSeries>();
                this.allSeries = new Dictionary<string, BetterSeries>();
                hoverSeries = null;
                randomColorState = new Random(int.Parse("0ddfaced", System.Globalization.NumberStyles.HexNumber));
                flaggedForDeletion = new List<string>();
                selectionCount = 0;
            }

            public PivotChart(XmlNode node) //this constructor is used for storage only, these charts are never displayed
            {
                partnerSeries = new Dictionary<string, BetterSeries>();
                this.allSeries = new Dictionary<string, BetterSeries>();
                hoverSeries = null;
                randomColorState = new Random(int.Parse("0ddfaced", System.Globalization.NumberStyles.HexNumber));
                flaggedForDeletion = new List<string>();
                selectionCount = 0;

                for (int i = 0; i < 2; i++)
                {
                    Chart c = new Chart();
                    ChartArea sumCount = new ChartArea();
                    sumCount.Name = "sum_count"; //current chart measures individual hex buckets, not sum_count, but I don't feel like changing it
                    sumCount.AxisX.ScaleView.Zoomable = true;
                    sumCount.AxisY.ScaleView.Zoomable = true;
                    sumCount.AxisY.Title = "Sample count";
                    sumCount.AxisX.Title = "Latency interval (2^x ns)";
                    Legend legend1 = new Legend();
                    legend1.Name = "Legend1";
                    c.ChartAreas.Add(sumCount);
                    c.Name = "Statistics";
                    c.Legends.Add(legend1);
                    c.TabIndex = 1;
                    c.Text = (i == 1 ? "hex_bins" : "sum_count");
                    XmlNode stats = safeSelectSingleNode(node, "pmbenchmark/report/statistics");
                    if (stats == null)
                    {
                        MessageBox.Show("PivotChart(" + node.Name + ") error: statistics node is null");
                        return;
                    }
                    writeHistogramToChart(c, stats, AccessType.read, Color.Blue, i == 1);
                    writeHistogramToChart(c, stats, AccessType.write, Color.Red, i == 1);
                    stats = null;
                    switch (i)
                    {
                        case (0):
                            shortChart = c;
                            break;
                        case (1):
                            fullChart = c;
                            break;
                        default:
                            MessageBox.Show("new pivot chart from xmlnode error: " + i);
                            break;
                    }
                }
        }

            public PivotChart(BenchPivot bp, int width, int height) //this constructor is used to produced the displayed chart 
            {
                partnerSeries = new Dictionary<string, BetterSeries>();
                this.allSeries = new Dictionary<string, BetterSeries>();
                hoverSeries = null;
                randomColorState = new Random(int.Parse("0ddfaced", System.Globalization.NumberStyles.HexNumber));
                flaggedForDeletion = new List<string>();
                selectionCount = 0;

                this.theBenchPivot = bp;
                chartPoint = theBenchPivot.chartPoint;

                for (int i = 0; i < 2; i++)
                {
                    string s = null;
                    Chart chart = new Chart();
                    ChartArea histogram = new ChartArea();
                    histogram.Name = "histogram";
                    histogram.AxisX.ScaleView.Zoomable = true;
                    histogram.AxisY.ScaleView.Zoomable = true;
                    histogram.AxisY.Title = "Sample count";
                    histogram.AxisX.Title = "Latency interval (2^x ns)" + s;
                    Legend legend1 = new Legend();
                    legend1.Name = "Legend";
                    legend1.DockedToChartArea = "histogram";
                    legend1.IsDockedInsideChartArea = true;

                    chart.ChartAreas.Add(histogram);
                    chart.Name = "Latency histogram (" + (i == 0 ? "short)" : "full)");
                    chart.Legends.Add(legend1);
                    chart.TabIndex = 1;
                    chart.Text = "histogram";

                    chart.MouseWheel += chartZoom;
                    chart.MouseEnter += chartMouseEnter;
                    chart.MouseLeave += chartMouseLeave;
                    chart.MouseMove += chartMouseHover;
                    chart.MouseClick += chartMouseClick;
                    chart.Width = width;
                    chart.Height = height;
                    switch (i)
                    {
                        case (0):
                            shortChart = chart;
                            break;
                        case (1):
                            fullChart = chart;
                            break;
                        default:
                            MessageBox.Show("New pivot chart error: " + i);
                            break;
                    }
                }
                if (shortChart == null || fullChart == null) MessageBox.Show("Error, null chart");
            }

            public bool setFull(bool b)
            {
                try
                {
                    showFull = hoverSeries.setFull(b);
                    currentChart = (showFull ? fullChart : shortChart);
                }
                catch (NullReferenceException) // x)
                {
                    MessageBox.Show("PivotChart.setFull(" + b.ToString() + ")null reference exception");
                }
                return showFull;
            }

            public void updateHoverSeries(Series s)
            {
                if (hoverSeries != null)
                {
                    hoverSeries.setSeriesEnabled(false);
                    if (s != null)
                    {
                        hoverSeries.setColor(s.Color);
                        try
                        {
                            currentChart.Series[hoverSeries.getSeriesName()].Points.Clear();
                            currentChart.DataManipulator.CopySeriesValues(s.Name, hoverSeries.getSeriesName());
                            hoverSeries.setSeriesEnabled(true);
                        }
                        catch (ArgumentException x)
                        {
                            MessageBox.Show("Argument exception. Series name is " + s.Name + ".\n" + x.ToString());
                        }
                    }
                }
                else
                {
                    hoverSeries = new HoverSeries(this);
                }
            }

            private static HitTestResult getHitTestResult(Chart c, Point mousepos, Point chartpoint)
            {
                chartpoint.X = mousepos.X;
                chartpoint.Y = mousepos.Y;
                return c.HitTest(c.PointToClient(chartpoint).X, c.PointToClient(chartpoint).Y);
            }
            private static void chartMouseEnter(object sender, EventArgs e)
            {
                Chart theChart = sender as Chart;
                if (!theChart.Focused) theChart.Focus();
            }

            public void chartMouseHover(object sender, EventArgs args)
            {
                HitTestResult htr = getHitTestResult(currentChart, Control.MousePosition, chartPoint);
                if (htr.ChartElementType == ChartElementType.LegendItem)
                {
                    if (htr.Object == null) { MessageBox.Show("PivotChart.chartMouseHover error: Null HTR"); }
                    else updateHoverSeries(currentChart.Series[(htr.Object as LegendItem).SeriesName]);
                }
                else updateHoverSeries(null);
            }

            private void toggleSelection(BetterSeries bs)
            {
                selectionCount += bs.toggleSelected(selectionCount);
            }

            public void refreshSelectionColors()
            {
                if (selectionCount < 0)  MessageBox.Show("refreshSelectionColors error: negative selction count"); 
                else
                {
                    Dictionary<string, BetterSeries>.ValueCollection.Enumerator checkus = allSeries.Values.GetEnumerator();
                    while (checkus.MoveNext())
                    {
                        checkus.Current.updateSelectionColor(selectionCount);
                    }
                    if (true) theBenchPivot.parserForBenchPivot.updateSelectionButtons(selectionCount);
                }
            }

            public void selectAll()
            {
                Dictionary<string, BetterSeries>.ValueCollection.Enumerator checkus = allSeries.Values.GetEnumerator();
                {
                    while (checkus.MoveNext())
                    {
                        if (!checkus.Current.getSelected()) toggleSelection(checkus.Current);
                    }
                }
                theBenchPivot.parserForBenchPivot.updateSelectionButtons(selectionCount);
            }
            public void selectNone()
            {
                Dictionary<string, BetterSeries>.ValueCollection.Enumerator checkus = allSeries.Values.GetEnumerator();
                {
                    while (checkus.MoveNext())
                    {
                        if (checkus.Current.getSelected()) toggleSelection(checkus.Current);
                    }
                }
            }

            private void chartMouseClick(object sender, EventArgs e)
            {
                MouseEventArgs mouseargs = (MouseEventArgs)e;
                HitTestResult htr = getHitTestResult(currentChart, Control.MousePosition, chartPoint);
                if (htr.ChartElementType == ChartElementType.LegendItem)
                {
                    if (htr.Object == null) { MessageBox.Show("PivotChart.chartMouseClick error: Null HTR"); }
                    else
                    {
                        LegendItem item = htr.Object as LegendItem;
                        switch (mouseargs.Button)
                        {
                            case (MouseButtons.Middle):
                                allSeries[item.SeriesName].theBenchRound.seriesObject.displaySpikes(); 
                                break;
                            case (MouseButtons.Right):
                                break;
                            case (MouseButtons.Left):
                                try
                                {
                                    BetterSeries bs = this.allSeries[item.SeriesName];
                                    toggleSelection(bs); 
                                    toggleSelection(partnerSeries[bs.getSeriesName()]);
                                    refreshSelectionColors(); 
                                }
                                catch (KeyNotFoundException) // x)
                                {
                                    MessageBox.Show("BenchPivot.PivotChart.toggleLegendItemSelected error: key " + item.SeriesName + " not found.");
                                }
                                break;
                        }
                    }
                }
                else { }
            }

            private static void chartZoom(object sender, MouseEventArgs args)
            {
                Chart theChart = sender as Chart;
                try
                {
                    Axis x = theChart.ChartAreas.FindByName("histogram").AxisX;
                    Axis y = theChart.ChartAreas.FindByName("histogram").AxisY;
                    double xMin = x.ScaleView.ViewMinimum, xMax = x.ScaleView.ViewMaximum;
                    double yMin = y.ScaleView.ViewMinimum, yMax = y.ScaleView.ViewMaximum;
                    double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
                    if (args.Delta < 0)
                    {
                        x1 = x.PixelPositionToValue(args.Location.X) - (xMax - xMin) * 2;
                        x2 = x.PixelPositionToValue(args.Location.X) + (xMax - xMin) * 2;
                        y1 = y.PixelPositionToValue(args.Location.Y) - (yMax - yMin) * 2;
                        y2 = y.PixelPositionToValue(args.Location.Y) + (yMax - yMin) * 2;

                    }
                    if (args.Delta > 0)
                    {
                        x1 = x.PixelPositionToValue(args.Location.X) - (xMax - xMin) / 2;
                        x2 = x.PixelPositionToValue(args.Location.X) + (xMax - xMin) / 2;
                        y1 = y.PixelPositionToValue(args.Location.Y) - (yMax - yMin) / 2;
                        y2 = y.PixelPositionToValue(args.Location.Y) + (yMax - yMin) / 2;
                    }
                    x.ScaleView.Zoom(x1, x2);
                    y.ScaleView.Zoom(y1, y2);
                }
                catch (Exception x) { MessageBox.Show("Zoom error:\n" + x.ToString()); return; }
            }

            private static void chartMouseLeave(object sender, EventArgs e)
            {
                Chart theChart = sender as Chart;
                if (theChart.Focused) theChart.Parent.Focus();
            }

            public Chart getPivotChart(DetailLevel detail)
            {
                switch (detail)
                {
                    case (DetailLevel.shorttype):
                        return shortChart;
                    case (DetailLevel.fulltype):
                        return fullChart;
                    case (DetailLevel.currenttype):
                        return currentChart;
                    default:
                        return null;
                }
            }

            private int getRandomColorState(bool read)
            {
                return randomColorState.Next() | (read ? int.Parse("ff0000a0", System.Globalization.NumberStyles.HexNumber) : int.Parse("ffff0000", System.Globalization.NumberStyles.HexNumber));
            }
            private void show(string s) { MessageBox.Show(s); }
            private Series collectDataPoints(BenchRound br, DetailLevel detail, AccessType type, int i) 
            {
                Series s = null;
                try
                {
                    string sname = (type == AccessType.read ? "read" : "write");
                    Chart chart = br.getRoundChart(detail);
                    s = new Series();
                    s.ChartArea = "histogram";
                    s.Legend = "Legend";
                    s.Name = theBenchPivot.getPivotDumpHeader(i) + " (" + sname + ")";
                    if (detail == DetailLevel.fulltype) br.registerSeriesName(s.Name, type);
                    DataPointCollection r = chart.Series[sname].Points;                  
                    DataPoint[] dp = new DataPoint[chart.Series[sname].Points.Count];
                    chart.Series[sname].Points.CopyTo(dp, 0);
                    for (int j = 0; j < dp.Length; j++)
                    {
                        s.Points.Insert(j, dp[j]);
                    }
                    s.ChartType = SeriesChartType.FastLine;
                    if (i <= 6) s.Color = (type == AccessType.read ? readColors[i] : writeColors[i]);
                    else s.Color = Color.FromArgb(getRandomColorState(type == AccessType.read));
                    s.BorderWidth = 2;
                    BetterSeries bs = allSeries[s.Name];
                    bs.setContainedSeries(s, type);
                }
                catch (KeyNotFoundException) // x)
                {
                    BetterSeries bs = new BetterSeries();
                    bs.setContainedSeries(s, type);
                    allSeries[s.Name] = bs;
                    bs.theBenchRound = br;
                    string pname = theBenchPivot.getPivotDumpHeader(i) + " (" + (type == AccessType.read ? "write" : "read") + ")";
                    partnerSeries.Add(pname, bs);
                }
                catch (ArgumentNullException x)
                {
                    MessageBox.Show("collectDataPoints null argument exception\n" + x.ToString());
                }
                catch (NullReferenceException x)
                {
                    MessageBox.Show("collectDataPoint null reference exception\n" + x.ToString());
                    return null;
                }
                return s;
            }

            public void addCollectedPoints(BenchRound br, AccessType s, int i)
            {
                Series tryshort = collectDataPoints(br, DetailLevel.shorttype, s, i);
                if (tryshort == null)
                {
                    throw (new ImportPmbenchException());
                }
                shortChart.Series.Add(tryshort);
                tryshort = collectDataPoints(br, DetailLevel.fulltype, s, i);
                if (tryshort == null)
                {
                    throw (new ImportPmbenchException());
                }
                fullChart.Series.Add(tryshort);
                return;
            }
        }

        private static class CsvWriter
        {
            private static string[] meminfos_headers = { "Pre-warmup", "Pre-run", "Mid-run", "Post-run", "Post-unmap" };
            private static string memitems_headers_linux = "Free KiB,Buffer KiB,Cache KiB,Active KiB,Inactive KiB,Page in/s,Page out/s,Swap in/s,Swap out/s,Major faults\n";
            private static string memitems_headers_windows = "AvailPhys,dwMemoryLoad,TotalPageFile,AvailPageFile,AvailVirtual\n";
            private static string results_headers = "Thread #,Net avg. (us),Net avg. (clk),Latency (us),Latency (clk),Samples,Overhead (us),Overhead (clk)\n"; //,Total\n";

            public static string getPivotDumpHeader(int i, BenchPivot bp) //i = crony #
            {
                        return bp.cronies[i].customName;
            }

            public static int writePivotCsvDump(string folder, BenchPivot bp, ref StreamWriter outfile)
            {
                string path = "";
                bool good = true;
                string csvfilename = null;

                if (folder == null)
                {
                    using (SaveFileDialog save = new SaveFileDialog())
                    {
                        save.Filter = "csv files (*.csv)|*.csv";
                        save.FilterIndex = 1;
                        save.RestoreDirectory = true;
                        save.AddExtension = true;
                        save.DefaultExt = "csv";
                        save.FileName = csvfilename;
                        save.InitialDirectory = Environment.SpecialFolder.UserProfile.ToString();
                        if (save.ShowDialog() == DialogResult.OK) { path = Path.GetFullPath(save.FileName); }
                        else { good = false; }
                    }
                }
                else { path = folder + "\\" + csvfilename + ".csv"; }
                if (good)
                {
                    try
                    {
                        outfile = new StreamWriter(path);
                        XmlNode report, result;
                        for (int h = 0; h < bp.cronies.Count; h++)
                        {
                            outfile.Write(getPivotDumpHeader(h, bp) + ",");
                            outfile.Write(results_headers);
                            report = safeSelectSingleNode(bp.cronies[h].roundNode, "pmbenchmark/report");
                            if (report == null)
                            {
                                bp.outfile.Write("writePivotCsvDump " + bp.cronies[h].customName + " report error\n");
                            }
                            else
                            {
                                for (int j = 1; j <= bp.cronies[h].jobs(); j++)
                                {
                                    result = safeSelectSingleNode(report, "result/result_thread[@thread_num='" + j + "']");
                                    if (result == null)
                                    {
                                        bp.outfile.Write("writePivotCsvDump: " + bp.cronies[h].customName + "'s thread " + j + " error\n");
                                    }
                                    else
                                    {
                                        bp.outfile.Write
                                        (
                                            " ," + j + "," +
                                            safeParseSingleNodeDouble(result, "result_netavg/netavg_us") + "," +
                                            safeParseSingleNodeDouble(result, "result_netavg/netavg_clk") + "," +
                                            safeParseSingleNodeDouble(result, "result_details/details_latency/latency_us") + "," +
                                            safeParseSingleNodeDouble(result, "result_details/details_latency/latency_clk") + "," +
                                            safeParseSingleNodeDouble(result, "result_details/details_samples") + "," +
                                            safeParseSingleNodeDouble(result, "result_details/details_overhead/overhead_us") + "," +
                                            safeParseSingleNodeDouble(result, "result_details/details_overhead/overhead_clk") + "\n" //"," + 
                                        );
                                    }
                                }
                            }
                        }
                        outfile.Write("\n");
                        report = null;
                        result = null;

                        List<XmlNode> histos;
                        bool first = true;
                        if (bp.pivotIndex == 6 || bp.baseParams.valueRatio > 0)
                        {
                            bp.outfile.Write("Read latencies,,");
                            histos = new List<XmlNode>();
                            for (int h = 0; h < bp.cronies.Count; h++)
                            {
                                if (bp.cronies[h].ratio() > 0)
                                {
                                    if (!first) { bp.outfile.Write(","); }
                                    else { first = false; }
                                    bp.outfile.Write(getPivotDumpHeader(h, bp));
                                    histos.Add(safeSelectSingleNode(bp.cronies[h].roundNode, "pmbenchmark/report/statistics/histogram[@type='read']"));
                                }
                            }
                            bp.outfile.Write("\n");
                            //MessageBox.Show("Writing histograms for " + histos.Count + " histograms");
                            writeCommaSeparatePivotHistogramList(histos, bp);
                            histos.Clear();
                        }
                        if (bp.pivotIndex == 6 || bp.baseParams.valueRatio < 100)
                        {
                            first = true;
                            bp.outfile.Write("Write latencies,,");
                            histos = new List<XmlNode>();
                            for (int h = 0; h < bp.cronies.Count; h++)
                            {
                                if (bp.cronies[h].ratio() < 100)
                                {
                                    if (!first) { bp.outfile.Write(","); }
                                    else { first = false; }
                                    bp.outfile.Write(getPivotDumpHeader(h, bp));
                                    histos.Add(safeSelectSingleNode(bp.cronies[h].roundNode, "pmbenchmark/report/statistics/histogram[@type='write']"));
                                }
                            }
                            bp.outfile.Write("\n");
                            writeCommaSeparatePivotHistogramList(histos, bp);
                            histos.Clear();
                        }
                        histos = null;

                        for (int m = 0; m < bp.cronies.Count; m++)
                        {
                            XmlNodeList sys_mem_items = bp.cronies[m].roundNode.SelectNodes("pmbenchmark/report/sys_mem_info/sys_mem_item");
                            int j = bp.cronies[m].cold();
                            if (bp.cronies[m].windowsbench())
                            {
                                bp.outfile.Write(getPivotDumpHeader(m, bp) + "," + memitems_headers_windows);
                                for (int k = 0; k < sys_mem_items.Count; k++)
                                {
                                    bp.outfile.Write(meminfos_headers[k + j] + ",");
                                    XmlNode item = sys_mem_items.Item(k);
                                    writeCommaSeparateMemInfoWindows(safeSelectSingleNode(item, "mem_item_info"), bp);
                                    if (!item.Attributes.Item(0).Value.Equals("post-unmap"))
                                    {
                                        bp.outfile.Write("Delta,");
                                        writeCommaSeparateMemInfoWindows(safeSelectSingleNode(item, "mem_item_delta"), bp);
                                    }
                                    item = null;
                                }
                            }
                            else
                            {
                                bp.outfile.Write(getPivotDumpHeader(m, bp) + "," + memitems_headers_linux);
                                for (int k = 0; k < sys_mem_items.Count; k++)
                                {
                                    bp.outfile.Write(meminfos_headers[k + j] + ",");
                                    XmlNode item = sys_mem_items.Item(k);
                                    writeCommaSeparateMemInfoLinux(safeSelectSingleNode(item, "mem_item_info"), bp);
                                    if (!item.Attributes.Item(0).Value.Equals("post-unmap"))
                                    {
                                        bp.outfile.Write("Delta,");
                                        writeCommaSeparateMemInfoLinux(safeSelectSingleNode(item, "mem_item_delta"), bp);
                                    }
                                    item = null;
                                }
                            }
                            sys_mem_items = null;
                        }
                        outfile.Flush();
                        outfile.Close();

                        if (folder == null) { MessageBox.Show("Wrote CSV to " + path); }
                    }
                    catch (IOException x) { MessageBox.Show("Error writing file to " + path + "\n" + x.ToString()); return 0; }
                }
                path = null;
                return 1;
            }

            private static void writeCommaSeparateMemInfoLinux(XmlNode info, BenchPivot bp)
            {
                try
                {
                    bp.outfile.Write
                    (
                        safeSelectSingleNode(info, "free_kib").InnerText + "," +
                        safeSelectSingleNode(info, "buffer_kib").InnerText + "," +
                        safeSelectSingleNode(info, "cache_kib").InnerText + "," +
                        safeSelectSingleNode(info, "active_kib").InnerText + "," +
                        safeSelectSingleNode(info, "inactive_kib").InnerText + "," +
                        safeSelectSingleNode(info, "pgpgin").InnerText + "," +
                        safeSelectSingleNode(info, "pgpgout").InnerText + "," +
                        safeSelectSingleNode(info, "pswpin").InnerText + "," +
                        safeSelectSingleNode(info, "pswpout").InnerText + "," +
                        safeSelectSingleNode(info, "pgmajfault").InnerText + "\n"
                    );
                }
                catch (NullReferenceException x)
                {
                    MessageBox.Show("BenchPivot.CsvWriter.writeCommaSeparateMemInfoLinux error: Null reference exception\n" + x.ToString());
                }
            }

            private static void writeCommaSeparateMemInfoWindows(XmlNode info, BenchPivot bp)
            {
                bp.outfile.Write
                (
                    safeSelectSingleNode(info, "AvailPhys").InnerText + "," +
                    safeSelectSingleNode(info, "dwMemoryLoad").InnerText + "," +
                    safeSelectSingleNode(info, "TotalPageFile").InnerText + "," +
                    safeSelectSingleNode(info, "AvailPageFile").InnerText + "," +
                    safeSelectSingleNode(info, "AvailVirtual").InnerText + "\n"
                );
            }

            private static void writeCommaSeparateFullBucket(List<XmlNode> nodes, int i, BenchPivot bp)
            {
                //write bucket i of all nodes in order
                double lo = Math.Pow(2, i + 7);
                double hi = Math.Pow(2, i + 8);
                double mid = (hi - lo) / 16;
                for (int j = 0; j < 16; j++) //bucket hexes with indexes 0-15
                {
                    double gap1 = lo + (j * mid);
                    double gap2 = gap1 + mid;
                    bp.outfile.Write(gap1 + "," + gap2 + ",");
                    for (int k = 0; k < nodes.Count; k++)
                    {
                        bp.outfile.Write(safeParseSingleNodeDouble(nodes[k], "histo_bucket[@index='" + i + "']/bucket_hexes/hex[@index='" + j + "']"));
                        if (k == nodes.Count - 1) { bp.outfile.Write("\n"); }
                        else { bp.outfile.Write(","); }
                    }
                }
            }

            private static void writeCommaSeparateSumCounts(XmlNode[] buckets, BenchPivot bp)
            {
                if (buckets == null)
                {
                    MessageBox.Show("writeCommaSeparateSumCounts error: null buckets");
                    return;
                }
                if (buckets[0] == null)
                {
                    MessageBox.Show("writeCommaSeparateSumCounts error: null first element");
                    return;
                }
                if (buckets[0].Attributes.Count == 0)
                {
                    MessageBox.Show("writeCommaSeparateSumCounts error: zero attributes");
                    return;
                }
                if (!buckets[0].Attributes.Item(0).Name.Equals("index"))
                {
                    MessageBox.Show("writeCommaSeparateSumCounts error: attribute name is " + buckets[0].Attributes.Item(0).Name);
                    return;
                }
                try
                {
                    int bucket_index = int.Parse(buckets[0].Attributes.Item(0).Value);
                    double lo, hi, interval_hi = safeParseSingleNodeInt(buckets[0], "bucket_interval/interval_hi");
                    double interval_lo = safeParseSingleNodeInt(buckets[0], "bucket_interval/interval_lo");
                    lo = Math.Pow(2, interval_lo);
                    hi = Math.Pow(2, interval_hi);
                    bp.outfile.Write(lo + "," + hi + ",");
                    for (int j = 0; j < buckets.Length; j++)
                    {
                        bp.outfile.Write(safeParseSingleNodeDouble(buckets[j], "sum_count"));
                        if (j == buckets.Length - 1) bp.outfile.Write("\n");
                        else bp.outfile.Write(",");
                    }
                }
                catch (ArgumentException x)
                {
                    MessageBox.Show("writeCommaSeparateSumCounts ArgumentException:\n" + x.ToString());
                    return;
                }
            }

            public static void writeCommaSeparatePivotHistogramList(List<XmlNode> nodes, BenchPivot bp)
            {
                //MessageBox.Show("writeCommaSeparatePivotHistogramList: Received a list of " + nodes.Count + " nodes");
                XmlNodeList[] bucket0s = new XmlNodeList[nodes.Count];
                bp.outfile.Write("0,256,");
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (nodes[i] == null)
                    {
                        MessageBox.Show("writeCommaseparatePivotHistogramList: Received null node at position " + i);
                        return;
                    }
                    //bucket0s[i] contains all of the bucket 0's for crony i
                    bucket0s[i] = nodes[i].SelectNodes("histo_bucket[@index='0']");
                    if (safeParseSingleNodeInt(bucket0s[i].Item(0), "bucket_interval/interval_lo") != 0)
                    {
                        MessageBox.Show("commaSeparateHistogramList: missing hit_counts_sum on test round " + i + 1);
                    }
                    bp.outfile.Write(safeParseSingleNodeDouble(bucket0s[i].Item(0), "sum_count"));
                    if (i == nodes.Count - 1)
                    {
                        bp.outfile.Write("\n");
                    }
                    else
                    {
                        bp.outfile.Write(",");
                    }
                }
                for (int i = 1; i < 16; i++) //buckets with indexes 1-15
                {
                    if (bp.showFull)
                    {
                        writeCommaSeparateFullBucket(nodes, i, bp);
                    }
                    else
                    {
                        XmlNode[] buckets = new XmlNode[nodes.Count];
                        for (int k = 0; k < nodes.Count; k++)
                        {
                            buckets[k] = safeSelectSingleNode(nodes[k], "histo_bucket[@index='" + i + "']");
                        }
                        writeCommaSeparateSumCounts(buckets, bp);
                    }
                }
                for (int i = 1; i < bucket0s[0].Count; i++) //skip the first sum_count
                {
                    try
                    {
                        XmlNode[] bucket0s_high = new XmlNode[nodes.Count];
                        for (int k = 0; k < nodes.Count; k++)
                        {
                            bucket0s_high[k] = bucket0s[k].Item(i);
                        }
                        writeCommaSeparateSumCounts(bucket0s_high, bp);
                    }
                    catch (IndexOutOfRangeException x)
                    {
                        MessageBox.Show("Index out of range exception at " + i.ToString() + " of " + bucket0s[0].Count + ":\n" + x.ToString());
                    }
                }
                bucket0s = null;
                bp.outfile.Write("\n");
            }
        }

        private PivotChart thePivotChart;
        protected int pivotIndex;
        protected ParamSet baseParams;
        public List<BenchRound> cronies;
        private bool chartReady;
        public PmGraph parserForBenchPivot;
        protected StreamWriter outfile;
        private static Color[] readColors = { Color.Blue, Color.Cyan, Color.Green, Color.MidnightBlue, Color.DarkCyan, Color.BlueViolet, Color.LimeGreen };
        private static Color[] writeColors = { Color.Red, Color.Fuchsia, Color.Orange, Color.SaddleBrown, Color.Maroon, Color.Chocolate, Color.HotPink };
        public bool dumped = false;
        private bool showFull { set; get; }
        public Point chartPoint;
        public enum AccessType { uninitialized = -1, read = 0, write = 1 }
        public enum DetailLevel { uninitialized = -1, shorttype = 0, fulltype = 1, currenttype = 2 }

        public void selectAll()
        {
            this.thePivotChart.selectAll();
        }

        private static string TextDialog(string prompt, string value)
        {
            Form dialog = new Form();
            dialog.Text = prompt;
            dialog.Height = 128;
            TextBox textbox = new TextBox();
            textbox.Text = value;
            textbox.Width = 256;
            textbox.Location = new Point (16, 16);
            Button ok = new Button(), cancel = new Button();
            ok.Name = ok.Text = "Submit";
            ok.Location = new Point(64, 48);
            ok.DialogResult = DialogResult.OK;
            dialog.AcceptButton = ok;
            cancel.Name = cancel.Text = "Cancel";
            cancel.Location = new Point(144, 48);
            dialog.CancelButton = cancel;
            dialog.Controls.AddRange(new Control[] { textbox, ok, cancel });
            DialogResult ret = dialog.ShowDialog();
            if (ret == DialogResult.OK) return value;
            return null;
        }

        public BenchSiblings averageSelected(int avgc)
        {
            parserForBenchPivot.updateSelectionButtons(0);
            XmlDocument doc = new XmlDocument();
            XmlNode fakeSeries = doc.CreateNode(XmlNodeType.Element, "test_nice", doc.NamespaceURI);
            doc.AppendChild(fakeSeries);
            ParamSet bp = new ParamSet();
            int flagcount = 0;
            try
            {
                for (int j = 0; j < cronies.Count; j++)
                {
                    if (cronies[j].flaggedForAverage)
                    {
                        XmlDocument tempdoc = cronies[j].roundNode.OwnerDocument;
                        XmlNode fakeRound = doc.CreateNode(XmlNodeType.Element, "test_round", doc.NamespaceURI);
                        XmlAttribute iter = doc.CreateAttribute("iter");
                        iter.Value = (flagcount++ + 1).ToString();
                        fakeRound.Attributes.Append(iter);
                        if (safeSelectSingleNode(tempdoc, "test_nice/test_round/pmbenchmark") == null)
                        {
                            MessageBox.Show("pmbenchmark node not found; root element is " + tempdoc.DocumentElement.Name);
                        }
                        fakeRound.AppendChild(doc.ImportNode(safeSelectSingleNode(tempdoc, "test_nice/test_round/pmbenchmark"), true));
                        fakeSeries.AppendChild(fakeRound);
                        bp.setParamsFromNode(PmGraph.getParamsNodeFromSeriesNode(fakeSeries));
                        bp.operatingSystem = safeSelectSingleNode(tempdoc, "test_nice/test_round/pmbenchmark/report/signature/pmbench_info/version_options").InnerText;
                    }
                }
            }
            catch (FileNotFoundException x)
            {
                MessageBox.Show("averageSelected:\n" + x.ToString());
                return null;
            }
            catch (ArgumentException x)
            {
                MessageBox.Show("averageSelected: ArgumentException\n" + x.ToString());
                return null;
            }
            try
            {
                BenchSiblings bs = new BenchSiblings(fakeSeries, doc, bp);            
                bs.trialsPerSeries = flagcount;
                string temp = "Average" + avgc++;
                string temp2 = TextDialog("Enter name for new average", temp);
                if (temp2 != null)
                {
                    if (bs.averageRound != null)
                    {
                        bs.averageRound.customName = temp2;
                    }
                    else
                    {
                        MessageBox.Show("averageSelected error: average round has null custom name");
                        return null;
                    }
                }
                else bs.averageRound.customName = temp;
                thePivotChart.selectNone();
                return bs;
            }
            catch (NullXmlNodeException) // x)
            {
                MessageBox.Show("Unable to average the selected benchmarks. One or more of them appears to be damaged.");
                return null;
            }
        }

        public int deleteSelected(bool nag)
        {
            bool debug = false;
            int deleted = thePivotChart.finalizeDeletions(thePivotChart.flaggedForDeletion.Count);
            if (deleted > 0)
            {
                for (int i = 0; i < cronies.Count; i++)
                {
                    if (cronies[i].hasPendingDeletions) cronies[i].deleteSelectedSeries(); 
                }
                int j = cronies.Count - 1;
                while (j >= 0)
                {
                    if (!cronies[j].hasReadSeries && !cronies[j].hasWriteSeries)
                    {
                        if (debug) MessageBox.Show(cronies[j].customName + " has neither a write nor a read series, deleting it now");
                        string s = cronies[j].customName;
                        cronies.RemoveAt(j);
                        parserForBenchPivot.removeDeadXmlDoc(s);
                    }
                    else if (debug) MessageBox.Show(cronies[j].customName + " still has a series"); 
                    j--;
                }
            }
            if (debug)
            {
                string cronytest = "";
                for (int i = 0; i < cronies.Count; i++)
                {
                    cronytest += cronies[i].customName + " has " + (cronies[i].hasReadSeries && cronies[i].hasWriteSeries ? "both" : (cronies[i].hasReadSeries ? "read" : (cronies[i].hasWriteSeries ? "write" : ""))) + "\n";
                }
                MessageBox.Show("Done deleting, pivot now has " + cronies.Count + " cronies:\n" + cronytest);
            }
            thePivotChart.refreshSelectionColors();
            return deleted;
        }

        public int markDeleteSelected(bool nag)
        {
            return thePivotChart.deleteFlagSelected(nag);
        }

        private bool setFull(bool b)
        {
            showFull = thePivotChart.setFull(b);
            return showFull;
        }

        public bool refreshFull(CheckBox b)
        {
            return (b == null ? setFull(showFull) : setFull(b.Checked));
        }

        public BenchPivot(ParamSet bp, int pivotindex, List<BenchRound> br, PmGraph parser)
        {
            this.thePivotChart = null;
            this.chartReady = false;
            this.baseParams = bp;
            this.pivotIndex = pivotindex;
            this.cronies = br;
            this.parserForBenchPivot = parser;
            chartPoint = new Point(); 
        }

        public string getPivotDumpHeader(int i) { return CsvWriter.getPivotDumpHeader(i, this);  }
        private static XmlNode safeSelectSingleNode(XmlNode n, string s) { return PmGraph.safeSelectSingleNode(n, s); }
        private static int safeParseSingleNodeInt(XmlNode n, string s) { return PmGraph.safeParseSingleNodeInt(n, s); }
        private static double safeParseSingleNodeDouble(XmlNode n, string s) { return PmGraph.safeParseSingleNodeDouble(n, s); }

        public Chart getPreparedChart(int w, int h, CheckBox b)
        {
            if (!chartReady)
            {
                Chart c = initializePivotChart(w, h, b);
                if (c == null)
                {
                    MessageBox.Show("getPreparedChart error: unable to generate chart");
                }
                return c;
            } 
            try
            {
                Chart pivotChart = thePivotChart.getPivotChart(DetailLevel.currenttype);
                pivotChart.Width = w;
                pivotChart.Height = h;
                return pivotChart;
            }
            catch (NullReferenceException x)
            {
                MessageBox.Show("BenchPivot.getPrepare_Chart info: null reference exception, the pivot chart is " + ((thePivotChart != null) ? "NOT" : "") + " null.\n" + x.ToString());
                return null;
            }
        }

        private Chart initializePivotChart(int width, int height, CheckBox b) 
        {
            if (this.cronies == null) { MessageBox.Show("BenchPivot.updateCharts Error: cronies list is empty!"); return null; }
            for (int i = 0; i < cronies.Count; i++)
            {
                if (cronies[i] == null) { MessageBox.Show("BenchPivot.updateCharts Error: crony " + i + " is null!"); return null; }
                cronies[i].getRoundChart(DetailLevel.currenttype);
            }
            thePivotChart = new PivotChart(this, width, height);
            for (int i = 0; i < cronies.Count; i++)
            {
                if (cronies[i].wasDeletedDontBother) continue;
                try
                {
                    if (cronies[i].ratio() > 0) thePivotChart.addCollectedPoints(cronies[i], AccessType.read, i);
                    if (cronies[i].ratio() < 100) thePivotChart.addCollectedPoints(cronies[i], AccessType.write, i);
                }
                catch (ImportPmbenchException) // x)
                {
                    MessageBox.Show("initializePivotChart(" + width + ", " + height + ", " + b.Checked.ToString() + ") error: unable to initialize pivot chart.");
                    return null;
                }
                catch (NullReferenceException x)
                {
                    MessageBox.Show("BenchPivot.initPivotChart(" + width + ", " + height + "): Null reference exception.\n(thePivotChart == null) == " + (thePivotChart == null).ToString() + "\n(cronies[" + i + "] == null) == " + (cronies[i] == null).ToString() + "\n" + x.ToString());
                }
            }
            thePivotChart.updateHoverSeries(null);
            setFull(true); //b.Checked);
            chartReady = (thePivotChart.currentChart != null);
            if (!chartReady) { MessageBox.Show("BenchPivot.initPivotChart(" + width + ", " + height + ") error: pivot chart is NOT ready."); }
            return thePivotChart.currentChart;
        }

        public int dumpPivotCsv(string folder)
        {
            return CsvWriter.writePivotCsvDump(folder, this, ref this.outfile);
        }

        public int getChartSelectionCount()
        {
            return thePivotChart.getSelectionCount();
        }

        public void destroyPivotChart()
        {
            if (thePivotChart != null)
            {
                if (chartReady)
                {
                    if (thePivotChart.currentChart == null) { MessageBox.Show("destroyChart Warning: Chart already destroyed"); return; }
                    thePivotChart.currentChart.Dispose();
                    thePivotChart.currentChart = null;
                    chartReady = false;
                }
                else { MessageBox.Show("BenchPivot.destroyPivotChart info: Chart is not ready"); }
            }
        }
    }

    public partial class PmGraph : Form
    {
        public partial class ControlPanel : FlowLayoutPanel
        {
            public Button averageSelectedButton, deleteSelectedButton, selectAllButton, selectNoneButton, helpButton;
            public CheckBox autoCheck, fullCheck;
            private static Padding controlPadding = new Padding(0, 6, 0, 0), panelPadding = new Padding(5, 0, 0, 0), actionButtonPadding = new Padding(3, 0, 0, 0);
            private static Size radioSize = new Size(13, 13), labelSize = new Size(72, 14);
            private PmGraph parser;
            private string importStandaloneDirectory = null;
            private Button exportManualButton, importManualSingleButton;

            private void helpButton_click(object sender, EventArgs e)
            {
                MessageBox.Show
               (
                    "Buttons:\n" +
                        "\tImport opens XML files produced by pmbench with the -f parameter;\n" +
                        "\tExport saves a CSV file reflecting the data presented in the graph;\n" +
                        "\tAverage adds the average of all selected items to the graph;\n" +
                        "\tDelete removes the selected items from the graph;\n" +
                        "\tSelect all selects all graphed items.\n\n" +
                    "Hover the mouse over a graphed item's name in the legend to highlight it;\n" +
                    "Left click an item's name to select it;\n"+
                    "Middle click an item's name to show its peak latencies."
               );
            }

            public ControlPanel(PmGraph p)
            {
                if (p == null) { MessageBox.Show("ControlPanel(null) Error"); return; }
                parser = p;
                //Padding checkPadding = new Padding(6, 8, 0, 0), checkLabelPadding = new Padding(0, 8, 0, 0); ;
                /*autoCheck = new CheckBox();
                autoCheck.Enabled = true;
                autoCheck.Size = radioSize;
                autoCheck.Margin = checkPadding;
                autoCheck.Checked = true;
                Label labelUpdate = new Label();
                labelUpdate.Text = "Auto update";
                labelUpdate.Enabled = true;
                labelUpdate.Size = new Size(65, 14);
                labelUpdate.Margin = checkLabelPadding;*/

                /*fullCheck = new CheckBox();
                fullCheck.Enabled = true;
                fullCheck.Size = radioSize;
                fullCheck.Margin = checkPadding;
                Label fullLabel = new Label();
                fullLabel.Text = "Show detailed results";
                fullCheck.Checked = true;
                fullLabel.Enabled = true;
                fullLabel.Margin = checkLabelPadding;
                fullCheck.CheckStateChanged += new EventHandler(parser.showFullChanged_action);*/

                Width = 220;
                Height = parser.Height;

                Controls.AddRange(new Control[]
                {
                initControlRow(new Control[]
                {
                    importManualSingleButton = initButton("Import", importSingleBenches_click, true),
                    exportManualButton = initButton("Export", parser.exportCsvManual, false)
                }, 220, 28, FlowDirection.LeftToRight, actionButtonPadding),
                initControlRow(new Control[]
                {
                    averageSelectedButton = initButton("Average selected", parser.averageSelectedButton_click, false),
                    deleteSelectedButton = initButton("Delete seleted", parser.deleteSelectedButton_click, false)
                }, 220, 28, FlowDirection.LeftToRight, actionButtonPadding),
                initControlRow(new Control[]
                {
                    selectAllButton = initButton("Select all", parser.selectAll_click, true),
                    helpButton = initButton("Instructions", helpButton_click, true)
                }, 220, 28, FlowDirection.LeftToRight, actionButtonPadding),
                /*initControlRow(new Control[]
                {
                    fullCheck, fullLabel
                }, 220, 28, FlowDirection.LeftToRight, actionButtonPadding)*/
                });
                FlowDirection = FlowDirection.TopDown;
                exportManualButton.Enabled = false;
            }

            private static Button initButton(string text, EventHandler e, bool enable)
            {
                Button b = new Button();
                b.Text = text;
                b.Click += new EventHandler(e);
                b.Enabled = enable;
                return b;
            }

            private FlowLayoutPanel initControlRow2(Control[] controls, int w, int h, FlowDirection d, Padding p)
            {
                FlowLayoutPanel flp = initControlRow(controls, w, h, d, p);
                return flp;
            }

            private FlowLayoutPanel initControlRow(Control[] controls, int w, int h, FlowDirection d, Padding p)
            {
                FlowLayoutPanel flp = new FlowLayoutPanel();
                flp.Width = w;
                flp.Height = h;
                flp.Controls.AddRange(controls);
                flp.FlowDirection = d;
                flp.Margin = p;
                return flp;
            }

            public void importSingleBenches_click(object sender, EventArgs args)
            {
                int before = 0;
                if (deleteSelectedButton.Enabled) before += 1;
                if (averageSelectedButton.Enabled) before += 4;
                setManualButtonsEnabled(false, 0);
                using (OpenFileDialog fd = new OpenFileDialog())
                {
                   if (importStandaloneDirectory != null) { fd.InitialDirectory = importStandaloneDirectory; }
                   else { fd.InitialDirectory = Environment.SpecialFolder.Desktop.ToString(); }
                   fd.Title = "Select benchmark(s) to import and average";
                   fd.Filter = "xml files (*.xml)|*.xml";
                   fd.Multiselect = true;
                   if (fd.ShowDialog() == DialogResult.OK)
                   {
                        parser.importSingle(fd.FileNames);
                   }
                }
                setManualButtonsEnabled(true, before);
            }

            public void setManualButtonsEnabled(bool t1, int i)
            {
                exportManualButton.Enabled = t1;
                importManualSingleButton.Enabled = t1;
                deleteSelectedButton.Enabled = (t1 && i > 0);
                averageSelectedButton.Enabled = (t1 && i > 2);
            }
        }

        private Dictionary<string, XmlDocument> xmlFiles;
        private Dictionary<string, BenchSiblings> allBenchSiblings;
        private Dictionary<string, BenchPivot> allBenchPivots;
        private Chart theChart;
        private FlowLayoutPanel flowPanel;
        public ControlPanel controlPanel;
        private BenchPivot theBenchPivot;
        private BenchPivot manualPivot = null;

        public PmGraph()
        {
            Point originPoint = new Point(0, 0);
            this.MinimumSize = new Size(800, 600);
            this.MaximumSize = new Size(Screen.GetWorkingArea(originPoint).Width, Screen.GetWorkingArea(originPoint).Height);
            this.Resize += new EventHandler(resize_event);
            flowPanel = new FlowLayoutPanel();
            flowPanel.Location = new Point(0, 0);
            flowPanel.Width = this.Width;
            flowPanel.Height = this.Height;
            flowPanel.FlowDirection = FlowDirection.LeftToRight;
            flowPanel.Controls.AddRange(new Control[] { controlPanel = new ControlPanel(this) });
            this.Controls.AddRange(new Control[] { flowPanel });
            xmlFiles = new Dictionary<string, XmlDocument>();
            allBenchSiblings = new Dictionary<string, BenchSiblings>();
            allBenchPivots = new Dictionary<string, BenchPivot>();
        }

        private void resize_event(object sender, EventArgs args)
        {
            flowPanel.Width = this.Width; flowPanel.Height = this.Height;
            if (flowPanel.Controls.Contains(theChart)) { updateChart(); } //conditional prevents updates during asyncronous loops
        }

        public void dropSelectionChanged_action(object o, EventArgs args) { dropSelectionChanged(controlPanel.autoCheck); }

        public void showFullChanged_action(object o, EventArgs args)
        {
            //MessageBox.Show("PmGraph.showFullChanged_action: details checkbox is " + (controlPanel.fullCheck.Checked ? "now" : "no longer") + " checked.");
            theBenchPivot.refreshFull(controlPanel.fullCheck);
            dropSelectionChanged(controlPanel.autoCheck);
        }

        public void dropSelectionChanged(CheckBox t)
        {
            if (t.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate () { dropSelectionChanged(t); });
            }
            else
            {
                if (t.Checked) { getResults(false); }
            }
        }

        public static XmlNode safeSelectSingleNode(XmlNode where, string xpath)
        {
            if (where == null)
            {
                throw new NullXmlNodeException();
            }
            try { return where.SelectSingleNode(xpath); }
            catch (System.Xml.XPath.XPathException x)
            {
                MessageBox.Show("XPath error:\n" + x.ToString()); return null;
            }
            catch (NullReferenceException x)
            {
                if (where != null)
                {
                    if (where.Name != null)
                    {
                        MessageBox.Show("safeSelectSingleNode(" + where.Name + ", " + xpath + "): Null reference exception:\n" + x.ToString());
                    }
                    else
                    {
                        MessageBox.Show("safeSelectSingleNode(NULL, " + xpath + "): Null reference exception:\n" + x.ToString());
                        return null;
                    }
                }
                else
                {
                    MessageBox.Show("safeSelectSingleNode(NULL, " + xpath + ") null reference exception: received input node is null");
                    return null;
                }
                return null;
            }
            catch (OutOfMemoryException) // x)
            {
                MessageBox.Show("Out of memory. You should probably just quit.");
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.RegisterForFullGCNotification(10, 10);
                //some kind of notification here
                while (true)
                {
                    if (GC.WaitForFullGCComplete() == GCNotificationStatus.Succeeded) { break; }
                    Thread.Sleep(500);
                }
                return safeSelectSingleNode(where, xpath);
            }
        }

        public static long safeParseSingleNodeLong(XmlNode where, string xpath)
        {
            if (where == null)
            {
                throw new NullXmlNodeException();
                //MessageBox.Show("(safeParseSingleNodeLong) Error: received null input node"); return 0;
            }
            long i = 0;
            try
            {
                if (safeSelectSingleNode(where, xpath) == null) { MessageBox.Show("(safeParseSingleNodeLong) xpath " + xpath + " returned a null reference"); }
                i = long.Parse(safeSelectSingleNode(where, xpath).InnerText);
            }
            catch (NullReferenceException x) { MessageBox.Show("Exception parsing integer from node inner text:\n" + x.ToString()); }
            catch (OverflowException x) { MessageBox.Show("(safeParseSingleNodeLong) Overflow exception at node " + xpath + ":\n" + x.ToString()); }
            return i;
        }

        public static int safeParseSingleNodeInt(XmlNode where, string xpath) //atoi(node.selectSingleNode(xpath)), with exception handling
        {
            if (where == null)
            {
                throw new NullXmlNodeException();
                //MessageBox.Show("(safeParseSingleNodeInt) Error: received null input node");
                //return 0;
            }
            int i = 0;
            try
            {

                if (safeSelectSingleNode(where, xpath) == null)
                {
                    MessageBox.Show("safeParseSingleNodeInt xpath " + xpath + " returned a null reference on node " + where.Name);
                }
                i = int.Parse(safeSelectSingleNode(where, xpath).InnerText);
            }
            catch (NullReferenceException x) { MessageBox.Show("Exception parsing integer from node inner text:\n" + x.ToString()); }
            catch (OverflowException x)
            {
                MessageBox.Show("(safeParseSingleNodeInt) Overflow exception at node " + xpath + ", with long value " + long.Parse(safeSelectSingleNode(where, xpath).InnerText) + ":\n" + x.ToString());
            }
            catch (FormatException x) { MessageBox.Show("(safeParseSingleNodeInt) Format exception at node " + where.Name + ", XPath " + xpath + ":\n" + x.ToString()); }
            return i;
        }

        public static double safeParseSingleNodeDouble(XmlNode where, string xpath)
        {
            if (where == null)
            {
                throw new NullXmlNodeException();
            }
            if (safeSelectSingleNode(where, xpath) == null)
            {
                return -1;
                //MessageBox.Show("(safeParseSingleNodeDouble) Null");
            }
            if (safeSelectSingleNode(where, xpath).InnerText == null)
            {
                return -1;
                //MessageBox.Show("SafeParseSingleNodeDouble Null inner text");
            }
            try
            {
                return double.Parse(safeSelectSingleNode(where, xpath).InnerText);
            } //throwing null exceptions because node selection is causing it to run out of memory
            catch (NullReferenceException x)
            {
                MessageBox.Show("(safeParseSingleNodeDouble(XmlNode, " + xpath + ") Null reference exception:\n" + x.ToString());
                return 0;
            }
        }

        private bool getResults(bool click)
        {
            if (!flowPanel.InvokeRequired) { removeChart(); }
            if (theBenchPivot == null) return false;
            updateChart();
            return true;
        }

        private void updateChart()
        {
            try
            {
                if (flowPanel == null) { MessageBox.Show("flow panel is null!"); return; }
                if (flowPanel.InvokeRequired) { return; }
                if (theBenchPivot == null) { MessageBox.Show("Bench pivot is null!"); return; }
                theChart = theBenchPivot.getPreparedChart(getChartWidth(), getChartHeight(), controlPanel.fullCheck);
                theChart.Location = new Point(controlPanel.Width + controlPanel.Margin.Left + 17, 44);
                flowPanel.Controls.Add(theChart);
            }
            catch (NullReferenceException) // x)
            {
                return;
            }
        }
        private int getChartWidth() { return flowPanel.Width - controlPanel.Width - controlPanel.Margin.Left - 17; }
        private int getChartHeight() { return flowPanel.Height - 44; }

        private bool removeChart()
        {
            if (flowPanel == null) { return false; }
            if (flowPanel.InvokeRequired) { MessageBox.Show("Illegal cross-thread chart removal attempted"); return false; }
            if (flowPanel.Controls.Contains(theChart))
            {
                flowPanel.Controls.Remove(theChart);
                theChart = null;
                return true;
            }
            return false;
        }

        public void exportCsv_click(object sender, EventArgs args) { if (getResults(false)) { dumpPivotCsv(null); } }
        private int dumpPivotCsv(string path) { return theBenchPivot.dumpPivotCsv(path); } //make sure to check getResults when calling this or theBenchPivot may be null
        private static string[] splitString(string s, char c) { char[] delimiter = { c }; return (s.Split(delimiter)); }
        public static XmlNode getParamsNodeFromSeriesNode(XmlNode node) { return safeSelectSingleNode(node, "test_round/pmbenchmark/report/signature/params"); }

        private bool writeXmlDocument(XmlDocument what, string where)
        {
            GC.WaitForPendingFinalizers();
            GC.Collect();
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlTextWriter xw = new XmlTextWriter(ms, System.Text.Encoding.Unicode))
                {
                    xw.IndentChar = '\t';
                    xw.Formatting = Formatting.Indented;
                    what.WriteContentTo(xw);
                    xw.Flush();
                    ms.Flush();
                    ms.Position = 0;
                    using (StreamReader sr = new StreamReader(ms))
                    {
                        try
                        {
                            File.WriteAllText(where, sr.ReadToEnd());
                            return true;
                        }
                        catch (IOException x) { MessageBox.Show("(writeXmlDocument) Error writing file to " + where + "\n" + x.ToString()); return false; }
                    }
                }
            }
        }

        private string registerXmlDocName(string s, XmlDocument doc, bool allowrename)
        {
            int trynum = 0;
            string t = s;
            while (true)
            {
                try
                {
                    xmlFiles.Add(t, doc);
                    break;
                }
                catch (ArgumentException) // x)
                {
                    if (allowrename) t = s + trynum++;
                }
            }
            //MessageBox.Show("registered " + t);
            return t;
        }

        public void removeDeadXmlDoc(string docname)
        {
            try
            {
                XmlDocument doc = xmlFiles[docname];
                xmlFiles.Remove(docname);
                //MessageBox.Show("Successfully removed dead XML doc " + docname);
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("removeDeadXmlDoc error: attempted to delete nonexistent XmlDocument " + docname);
            }
        }

        public void renameXmlDoc(string oldname, string newname)
        {

        }

        public void importSingle(string[] filenames)
        {
            for (int j = 0; j < filenames.Length; j++)
            {
                try
                {
                    XmlDocument tempdoc = new XmlDocument();
                    tempdoc.Load(filenames[j]);
                    XmlDocument doc = new XmlDocument();
                    XmlNode fakeSeries = doc.CreateNode(XmlNodeType.Element, "test_nice", doc.NamespaceURI);
                    doc.AppendChild(fakeSeries);
                    ParamSet bp = new ParamSet();
                    XmlNode fakeRound = doc.CreateNode(XmlNodeType.Element, "test_round", doc.NamespaceURI);
                    XmlAttribute iter = doc.CreateAttribute("iter");
                    iter.Value = ("1").ToString();
                    fakeRound.Attributes.Append(iter);
                    XmlNode catchnode = doc.ImportNode(safeSelectSingleNode(tempdoc, "pmbenchmark"), true);
                    if (catchnode == null)
                    {
                        MessageBox.Show("inmportSingle error: " + filenames[j] + " is malformed.");
                        return;
                    }
                    fakeRound.AppendChild(catchnode);
                    fakeSeries.AppendChild(fakeRound);
                    bp.setParamsFromNode(getParamsNodeFromSeriesNode(fakeSeries));
                    XmlNode osnode = safeSelectSingleNode(tempdoc, "pmbenchmark/report/signature/pmbench_info/version_options");
                    if (osnode == null)
                    {
                        MessageBox.Show("inmportSingle error: " + filenames[j] + " is missing version options.");
                        return;
                    }
                    bp.operatingSystem = osnode.InnerText;
                    BenchSiblings bs = new BenchSiblings(fakeSeries, doc, bp);
                    if (bs.initialized == false)
                    {
                        MessageBox.Show("importSingle error: unable to initialize bench siblings");
                        return;
                    }
                    string[] splat1 = splitString(filenames[j], '\\');
                    string[] splat2 = splitString(splat1[splat1.Length - 1], '.');
                    bs.averageRound.customName = registerXmlDocName(splat2[0], doc, true);
                    addSeriesAverageToManualPivot(bs);
                }
                catch (XmlException x)
                {
                    MessageBox.Show("importSingle XmlException:\n " + x.ToString());
                    return;              
                }
            }
        }

        public void addSeriesAverageToManualPivot(BenchSiblings addme)
        {
            if (manualPivot == null)
            {
                List<BenchRound> br = new List<BenchRound>();
                br.Add(addme.averageRound);
                manualPivot = new BenchPivot(addme.benchParams, 9, br, this);
            }
            else manualPivot.cronies.Add(addme.averageRound);
            graphManual();
        }

        public void graphManual()
        {
            if (manualPivot == null)
            {
                MessageBox.Show("manual pivot is null, returning");
                return;
            }
            removeChart();
            if (manualPivot == null) { MessageBox.Show("PmGraph.graphManual: manualPivot is null after PmGraph.removeChart()."); }
            if (flowPanel.Controls.Contains(theChart)) { MessageBox.Show("graphManual redundant flowpanel chart removal error"); }
            if (manualPivot == null) { MessageBox.Show("PmGraph.graphManual: manualPivot is null after flowpanel chart removal."); }
            manualPivot.destroyPivotChart(); //forgot why this is necessary
            theChart = manualPivot.getPreparedChart(getChartWidth(), getChartHeight(), controlPanel.fullCheck);
            if (theChart == null) { MessageBox.Show("PmGraph.graphManual: chart was assigned as null."); }
            try
            {
                theChart.Location = new Point(controlPanel.Width + controlPanel.Margin.Left + 17, 44);
                flowPanel.Controls.Add(theChart);
                theBenchPivot = manualPivot;
            }
            catch (NullReferenceException x)
            {
                MessageBox.Show("PmGraph.graphManual: Null reference exception.\n" + "theChart is " + (theChart == null ? "INDEED" : "NOT") + " null.\n" + x.ToString());
            }
        }

        private int averageCounter = 0;
        private void averageSelectedButton_click(object sender, EventArgs e)
        {
            BenchSiblings bs = theBenchPivot.averageSelected(averageCounter++);
            if (bs != null) { addSeriesAverageToManualPivot(bs); }
        }

        private bool nag = true;
        private void deleteSelectedButton_click(object sender, EventArgs e)
        {
            theBenchPivot.markDeleteSelected(nag);
            theBenchPivot.deleteSelected(nag);
        }

        public void exportCsvManual(object sender, EventArgs e)
        {
            manualPivot.dumpPivotCsv(null);
        }

        public void updateSelectionButtons(int i)
        {
            controlPanel.setManualButtonsEnabled(true, i);
        }

        public bool doesPivotHaveSelections()
        {
            return (theBenchPivot == null ? false : theBenchPivot.getChartSelectionCount() > 0);
        }

        public void selectAll_click(object sender, EventArgs e)
        {
            if (theBenchPivot == null) return;
            theBenchPivot.selectAll();
        }
    }
}
